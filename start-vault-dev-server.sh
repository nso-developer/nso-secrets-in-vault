#!/bin/sh
export VAULT_ADDR='http://127.0.0.1:8200'
export VAULT_TOKEN=blabla
vault server --dev \
	-dev-root-token-id=blabla \
	-dev-listen-address=127.0.0.1:8200 &

sleep 1

vault kv put secret/NSO/encryption-keys \
     DES3CBC_KEY1=0123456789abcdef \
     DES3CBC_KEY2=0123456789abcdef \
     DES3CBC_KEY3=0123456789abcdef \
     DES3CBC_IV=0123456789abcdef \
     AESCFB128_KEY=0123456789abcdef0123456789abcdef \
     AESCFB128_IV=0123456789abcdef0123456789abcdef \
     AES256CFB128_KEY=0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef 