#!/usr/bin/env python3
# -*- mode: python; python-indent: 4 -*-
import ncs
from _ncs import decrypt
from ncs.application import Service
from ncs.dp import Action
import requests
import os
import sys
import json

def get_credentials(token, authgroup):
    return get_key_values(token, 'authgroup/' + authgroup)

def get_encryption_keys(token):
    return get_key_values(token, 'encryption-keys')

def get_key_values(token, path):
    assert 'VAULT_ADDR' in os.environ, 'VAULT_ADDR environment variable not set'
    VAULT_ADDR= os.environ['VAULT_ADDR']
    MOUNT_POINT='secret'
    NSO_PATH = 'NSO'
    url = f'{VAULT_ADDR}/v1/{MOUNT_POINT}/data/{NSO_PATH}/{path}'
    headers = { "X-Vault-Token" : token
        , "X-Vault-Request" : 'true'
        , "Content-Type" : "application/json"}
    r = requests.get(url, headers=headers)
    assert r.ok, f'Could not retrieve vault key values at url:{url} reason: {r.reason}'
    return json.loads(r.content)['data']['data']

# ---------------
# ACTIONS EXAMPLE
# ---------------
class GetCredentialsAction(Action):
    @Action.action
    def cb_action(self, uinfo, name, kp, input, output, trans):
        self.log.info('action name: ', name)
        self.log.info(f'local user:{input.local_user} authgroup:{input.authgroup} device:{input.device}')
        with ncs.maapi.Maapi() as maapi:
            with ncs.maapi.Session(maapi, 'admin', 'python'):
                with maapi.start_read_trans() as t:
                    root = ncs.maagic.get_root(t)
                    maapi.install_crypto_keys()
                    assert root.vault.token is not None, "Vault token not set"
                    token = decrypt(root.vault.token)
        response = get_credentials(token, input.authgroup)
        output.remote_user = response['user']
        output.remote_password = response['password']
        if 'secondary-password' in response:
            output.remote_secondary_password = response['secondary-password']




# ---------------------------------------------
# COMPONENT THREAD THAT WILL BE STARTED BY NCS.
# ---------------------------------------------
class Main(ncs.application.Application):
    def setup(self):
        # The application class sets up logging for us. It is accessible
        # through 'self.log' and is a ncs.log.Log instance.
        self.log.info('Main RUNNING')


        # When using actions, this is how we register them:
        #
        self.register_action('hashicorp-vault-get-credentials', GetCredentialsAction)

        # If we registered any callback(s) above, the Application class
        # took care of creating a daemon (related to the service/action point).

        # When this setup method is finished, all registrations are
        # considered done and the application is 'started'.

    def teardown(self):
        # When the application is finished (which would happen if NCS went
        # down, packages were reloaded or some error occurred) this teardown
        # method will be called.

        self.log.info('Main FINISHED')

if __name__ == '__main__':
    try:
        assert 'VAULT_TOKEN' in os.environ,'Need to set VAULT_TOKEN environment variable'
        keys = get_encryption_keys(os.environ['VAULT_TOKEN'])
    except Exception as err:
        print(f'ERROR={str(err)}')
        sys.exit(1)
    for (k,v) in keys.items():
        print(f'{k}={v}')
    sys.exit(0)
