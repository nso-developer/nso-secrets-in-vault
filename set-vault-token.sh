#!/bin/sh
export VAULT_ADDR="${VAULT_ADDR:-http://127.0.0.1:8200}"
export VAULT_TOKEN="${VAULT_TOKEN:-blabla}"
cat <<EOF > nso-authgroup-read-policy.hcl
path "secret/data/NSO/authgroup/*"
{
 capabilities = ["read"]
}
EOF
vault policy write nso-authgroup-read nso-authgroup-read-policy.hcl
TOKEN=`vault token create  -policy="nso-authgroup-read" -format=json | jq .auth.client_token`
echo Token is $TOKEN
ncs_cli -Cu admin <<EOF
config
vault token $TOKEN
commit
EOF