# NSO secrets in vault

## Overview

This Cisco NSO service package enables you to retrieve device credentials from Hashicorp vault, it
also allows NSO to retrieve  encryption keys that are used to decrypt any encrypted datatypes
in NSO's cdb from vault.
The intention is to simplify storing the full NSO state in your source code repository without
exposing secrets to anyone who can read that repository.

To use the package you configure one or more authgroups as follows

    admin@ncs(config)# devices authgroups group external-credentials
    admin@ncs(config-group-external-credentials)# default-map callback-node /vault
    admin@ncs(config-group-external-credentials)# default-map action-name get-credentials
    admin@ncs(config-group-external-credentials)# commit
    Commit complete.

Note that the `get-credentials` action referenced above is hidden so you cannot just get credentials from vault by calling it from the NSO NBI.

You also have to configure a token which is used to autenticate towards vault. The token is stored encrypted in cdb. More details about getting the token are below.

Then when connecting to a device associated with those authgroups, NSO will get the device credentials from vault.

For an environment where your complete NSO configuration is stored in a source code repository, as is typical for CI/CD, the device credentials are no longer part of that configuration and will never be exposed via that repository.

The package optionally lets you store the encryption keys in vault.
These are the keys that are used to decrypt any of the encrypted values stored in NSO's cdb.
Normally these keys are in `ncs.conf`. Having the in vault, means you can also store your
`ncs.conf` configuration file in git source control without exposiong those keys and without
having to write code to build that file, i.e no need to inject the keys into the file.

## Prerequisites

Install the vault client. You will store credentials in vault using that client. The NSO package only reads the credentials using the vault REST API so does not actually use the client.

Download vault here https://www.vaultproject.io/docs/install . This includes client and server in one executable

For initial testing start the dev server. THe following script starts adevelopment server with a
hard coded root key, using http rather than https - not for production.

    $ source start-vault-dev-server.sh

The script also sets the `VAULT_ADDR` and `VAULT_TOKEN` environment variables. `VAULT_ADDR` is used as the base of the URL to access vault. `VAULT_TOKEN` is used as the token to authenticate to vault by the vault client.

THe environment variables need to be set before starting `ncs`. While `VAULT_ADDR` is mandatory,
`VAULT_TOKEN` is only used by `ncs` when storing the encryption keys in vault.

The package stores credentials in a vault kv-v2 secrets engine that is mounted on the path 'secret'. This is enabled using the vault client (the development server does this for you by default)

    $ vault secrets enable -path=secret kv-v2

The full path to the credentials of an authgroup is then 
`secret/data/NSO/authgroup/<authgroup name>`,
the credentials are stored as key value pairs, the keys are `user`, `password`, and optionally
`secondary-password` which is the enable password.
For an authgroup `external-credentials` you would use the following command.

    $ vault kv put secret/NSO/authgroup/external-credentials \
    	user=myuser password=MYpass secondary-password=MYenablePass
    
The script

    $ ./set-vault-token.sh

can be used to create a vault token, and configures it in NSO using the cli interface.
It uses a policy stored in `nso-authgroup-read-policy.hcl`, which only grants access to read at
the location used to store credentials.

    $ cat nso-authgroup-read-policy.hcl
    path "secret/data/NSO/authgroup/*"
    {
    	capabilities = ["read"]
    }

The script loads the above file in a policy `nso-authgroup-read`,
then generates a token for that policy. Below we see the commands to do the same manually.

    $ vault policy write nso-authgroup-read nso-authgroup-read-policy.hcl
    Success! Uploaded policy: nso-authgroup-read
    $ vault token create  -policy="nso-authgroup-read"

And the NSO CLI command to confiugure a token

    admin@ncs(config)# vault token s.pL77Q6rslsbLm4DQ68bF9j8z
    admin@ncs(config)# commit
    Commit complete.


## Storing Encryption keys

You can also store your encryption keys in vault as follows

    $ vault kv put secret/NSO/encryption-keys \
    > DES3CBC_KEY1=0123456789abcdef \
    > DES3CBC_KEY2=0123456789abcdef \
    > DES3CBC_KEY3=0123456789abcdef \
    > DES3CBC_IV=0123456789abcdef \
    > AESCFB128_KEY=0123456789abcdef0123456789abcdef \
    > AESCFB128_IV=0123456789abcdef0123456789abcdef \
    > AES256CFB128_KEY=0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef

The above are the default values in the NSO development install, typically you use random values.

Then if you have the package installed and unpacked (not as `*.tar.gz`) and you include the following in `ncs.conf`

    <encrypted-strings>
      <external-keys>
        <command>packages/nso-secrets-in-vault/python/hashicorp_vault/main.py</command>
      </external-keys>
    </encrypted-strings>

Instead of having the keys explicitly in `ncs.conf` then when you run `ncs --reload`, ncs will fetch its encryption keys from vault.

You will need to have in addition to the `VAULT_ADDR` environment variable also a `VAULT_TOKEN` variable with a vault token that has read access to the path `secret/data/NSO/encryption-keys`. The variables needs to be set before starting `ncs`

## Install

    % cd packages
    % git clone git@gitlab.com:snovello/nso-secrets-in-vault.git
    % make -C nso-secrets-in-vault/src all
    % ncs_cli -Cu admin
    admin@ncs# packages reload

## Design

This package was developed using vault 1.7.0 and NS) 5.5. There is no automated set of tests for it.

Used `request` rather that the `hvac` python module which implements API to vault since the only API request is to read a secret. It seemed unnecessary to get a whole library to just make one REST call.

Kept the `VAULT_ADDR` as an environment variable to not have it visible in the API and to make it harder to point NSO to a man-in-the-middle, and because its used on start up for the encryption keys.

Chose to store read access token, encrypted in DB rather than just have it in an environment variable to make it easier to change the token regularly without restarting NSO so as to enable using short lived tokens.

The package is aimed at 2 simple use cases, keeping the southbound credentials for devices and the encryption keys in vault. It would have been possible to make it more generic to read anything from vault or interact with any part of the vault API, but without the specific use cases to drive the design I chose to leave that out.


